#/bin/bash

cp ../huckel_triangulene.py .
chmod +x library_template.py
./library_template.py > library_tests.log

cd GNI_7x1-0-0
LIS1=$(ls -R)
cd ..

cd ../Examples/GNI_7x1-0-0
LIS2=$(ls -R)
cd ..

cd ../Tests
rm huckel_triangulene.py
rm -R __pycache__

if [[ $LIS1 == $LIS2 ]]; then
	echo "Final test is a success!" >> library_tests.log
else
	echo "Something failed with final test!" >> library_tests.log
fi