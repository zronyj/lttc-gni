#!/usr/bin/env python3

import numpy as np
import huckel_triangulene as gni

# Simple first test with triangulene
print("Building a simple (7|0,0,0) triangulene (just coordinates) ... ", end="")
mol = gni.build_triangle(7)

ref = [[0, 0, 1], [0, -1, -1], [0, 1, -1], [1, -1, 1], [1, 1, 1], [1, -2, -1], [1, 0, -1], [1, 2, -1], [2, -2, 1], [2, 0, 1], [2, 2, 1], [2, -3, -1], [2, -1, -1], [2, 1, -1], [2, 3, -1], [3, -3, 1], [3, -1, 1], [3, 1, 1], [3, 3, 1], [3, -4, -1], [3, -2, -1], [3, 0, -1], [3, 2, -1], [3, 4, -1], [4, -4, 1], [4, -2, 1], [4, 0, 1], [4, 2, 1], [4, 4, 1], [4, -5, -1], [4, -3, -1], [4, -1, -1], [4, 1, -1], [4, 3, -1], [4, 5, -1], [5, -5, 1], [5, -3, 1], [5, -1, 1], [5, 1, 1], [5, 3, 1], [5, 5, 1], [5, -6, -1], [5, -4, -1], [5, -2, -1], [5, 0, -1], [5, 2, -1], [5, 4, -1], [5, 6, -1], [6, -6, 1], [6, -4, 1], [6, -2, 1], [6, 0, 1], [6, 2, 1], [6, 4, 1], [6, 6, 1], [6, -7, -1], [6, -5, -1], [6, -3, -1], [6, -1, -1], [6, 1, -1], [6, 3, -1], [6, 5, -1], [6, 7, -1], [7, -7, 1], [7, -5, 1], [7, -3, 1], [7, -1, 1], [7, 1, 1], [7, 3, 1], [7, 5, 1], [7, 7, 1], [7, -6, -1], [7, -4, -1], [7, -2, -1], [7, 0, -1], [7, 2, -1], [7, 4, -1], [7, 6, -1]]

if mol == ref:
	print("Success!")
else:
	print("Failed!")

# Test for GNI
print("Building a (7|2,1,0) GNI (just coordinates) ... ", end="")
mol = gni.build_triangle(7)
mol = gni.rem_lambda(2, 7, mol, 0)
mol = gni.rem_lambda(1, 7, mol, 1)

ref = [[0, 0, 1], [0, -1, -1], [0, 1, -1], [1, -1, 1], [1, 1, 1], [1, -2, -1], [1, 0, -1], [1, 2, -1], [2, -2, 1], [2, 0, 1], [2, 2, 1], [2, -3, -1], [2, -1, -1], [2, 1, -1], [2, 3, -1], [3, -3, 1], [3, -1, 1], [3, 1, 1], [3, 3, 1], [3, -4, -1], [3, -2, -1], [3, 0, -1], [3, 2, -1], [3, 4, -1], [4, -4, 1], [4, -2, 1], [4, 0, 1], [4, 2, 1], [4, 4, 1], [4, -5, -1], [4, -3, -1], [4, -1, -1], [4, 1, -1], [4, 3, -1], [4, 5, -1], [5, -5, 1], [5, -3, 1], [5, -1, 1], [5, 1, 1], [5, 3, 1], [5, 5, 1], [5, -4, -1], [5, -2, -1], [5, 0, -1], [5, 2, -1], [5, 4, -1], [5, 6, -1], [6, -4, 1], [6, -2, 1], [6, 0, 1], [6, 2, 1], [6, 4, 1], [6, 6, 1], [6, -3, -1], [6, -1, -1], [6, 1, -1], [6, 3, -1], [6, 5, -1], [7, -3, 1], [7, -1, 1], [7, 1, 1], [7, 3, 1], [7, 5, 1], [7, -2, -1], [7, 0, -1], [7, 2, -1], [7, 4, -1]]

if mol == ref:
	print("Success!")
else:
	print("Failed!")

# Matrix construction
print("Building the Huckel matrix for the (4|2,1,0) GNI ... ", end="")
mol = gni.build_triangle(4)
mol = gni.rem_lambda(2, 4, mol, 0)
mol = gni.rem_lambda(1, 4, mol, 1)
mat = gni.build_matrix(mol)

ref = np.array([[0., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [1., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [1., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 1., 0., 0., 0., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 1., 0., 0., 0., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 1., 1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 1., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 0., 1., 0., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 0., 0., 0., 1., 0., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 0., 0., 0., 1., 0.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 0., 1.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 1.],
       [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 0.]])

if np.array_equal(mat, ref):
	print("Success!")
else:
	print("Failed!")

# Matrix diagonalization
print("Diagonalizing the Huckel matrix for the (4|2,1,0) GNI ... ", end="")
mol = gni.build_triangle(4)
mol = gni.rem_lambda(2, 4, mol, 0)
mol = gni.rem_lambda(1, 4, mol, 1)
mat = gni.build_matrix(mol)
eva, eve = np.linalg.eigh(mat)

ref = np.array([-2.6259971995999027, -2.2613399923334665, -1.9659266495478822,
-1.7772592155324887, -1.534284525926745, -1.2323936434143317, -1.2099005546144095,
-1.1035134764261223, -0.8748201001757062, -0.7500469262956834, -0.2909590921682307,
0.29095909216823146, 0.7500469262956841, 0.8748201001757072, 1.1035134764261219,
1.2099005546144104, 1.232393643414332, 1.5342845259267437, 1.7772592155324884,
1.9659266495478804, 2.2613399923334674, 2.6259971995999036])

if np.array_equal(eva, ref):
	print("Success!")
else:
	print("Failed!")

# Complete test
print("Running a full calculation where files are saved and graphs are plotted.")
gni.GNI(7, 1, 0, 0, savexyz=True, savemat=True, saveeigen=True, savemol=True,
		showplot=False, orbitals=True, force=True)