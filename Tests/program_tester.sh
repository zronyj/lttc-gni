#/bin/bash

echo -n "Testing for (3|0,0,0) and saving the plot and red-blue molecule PNG file ... "
../huckel_triangulene.py -L 3 -c 0 0 0 --mol -F >/dev/null 2>&1
if cmp --silent ./GNI_3x0-0-0/molecule.png ../Examples/GNI_3x0-0-0/molecule.png; then
	echo "Success!"
else
	echo "Failed!"
fi

echo -n "Testing for (5|0,0,0) and saving the plot and XYZ file ... "
../huckel_triangulene.py -L 5 -c 0 0 0 --xyz -F >/dev/null 2>&1
if cmp --silent ./GNI_5x0-0-0/GNI_5x0-0-0_coords.xyz ../Examples/GNI_5x0-0-0/GNI_5x0-0-0_coords.xyz; then
	echo "Success!"
else
	echo "Failed!"
fi

echo -n "Testing for (7|2,0,0) and saving the plot and matrix ... "
../huckel_triangulene.py -L 7 -c 2 0 0 --mat -F >/dev/null 2>&1
if cmp --silent ./GNI_7x2-0-0/GNI_7x2-0-0_huckel.csv ../Examples/GNI_7x2-0-0/GNI_7x2-0-0_huckel.csv; then
	echo "Success!"
else
	echo "Failed!"
fi

echo -n "Testing for (7|2,2,2) and saving the plot and eigenvalues and eigenvectors files ... "
../huckel_triangulene.py -L 7 -c 2 2 2 --eig -F >/dev/null 2>&1
FIL1=$(cmp --silent ./GNI_7x2-2-2/GNI_7x2-2-2_eigvals.dat ../Examples/GNI_7x2-2-2/GNI_7x2-2-2_eigvals.dat; echo $?)
FIL2=$(cmp --silent ./GNI_7x2-2-2/GNI_7x2-2-2_eigvecs.dat ../Examples/GNI_7x2-2-2/GNI_7x2-2-2_eigvecs.dat; echo $?)
if [[ $FIL1 == 0 && $FIL2 == 0 ]]; then
	echo "Success!"
else
	echo "Failed!"
fi

echo -n "Testing for (4|0,0,0) and saving everything ... "
../huckel_triangulene.py -L 4 -c 0 0 0 --xyz --mol --mat --eig --orb -F >/dev/null 2>&1
LIS1=$(ls ./GNI_4x0-0-0/)
LIS2=$(ls ../Examples/GNI_4x0-0-0/)
if [[ $LIS1 == $LIS2 ]]; then
	echo "Success!"
else
	echo "Failed!"
fi