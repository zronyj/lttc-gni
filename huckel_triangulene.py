#!/usr/bin/env python3

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Description <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#
#          ** HuckEn GNI: Triangulene/GNI Huckel Energy Calculator **
#
# This program calculates the energy of a triangulene or a graphene
# nanoisland -GNI- using a Huckel matrix, and diagonalizes it.
# -----------------------------------------------------------------------------
# Brefore proceeding to understand the program, please consider the following
# diagram. This will ease the process of understanding both the numbering and
# the way the triangulene is built.
#
#                                    .·R·.
#                                   B     B
#                                   |     |
#                                 .·R·. .·R·.
#                                B     B     B
#                                |     |     |
#                              .·R·. .·R·. .·R·.
#                             B     B     B     B
#                             |     |     |     |
#                             R·. .·R·. .·R·. .·R
#                                B     B     B
#
# The previous figure can be flattened and each position can be represented in
# a 2D coordinate system in the following way:
#
#                  Position  -3 -2 -1  0  1  2  3
#            Layer            .  .  .  .  .  .  .
#              1 · · · · · · ·.  .  B--R--B  .  .
#                             .  .  |     |  .  .
#              2 · · · · · · ·.  B--R--B--R--B  .
#                             .  |     |     |  .
#              3 · · · · · · ·B--R--B--R--B--R--B
#                             |     |     |     |
#              4 · · · · · · ·R--B--R--B--R--B--R
#
# To better identify the blue (B) and red (R) carbon atoms, -1 and 1 were used
# respectively as a third coordinate. Therefore, the triangulene in this figure
# can be represented as a list of 3D coordinates:
#
#                           Layer  Position   Color
#                               0      0      1
#                               0     -1     -1
#                               0      1     -1
#                               1     -1      1
#                               1      1      1
#                               1     -2     -1
#                               1      0     -1
#                               1      2     -1
#                               2     -2      1
#                               2      0      1
#                               2      2      1
#                               2     -3     -1
#                               2     -1     -1
#                               2      1     -1
#                               2      3     -1
#                               3     -3      1
#                               3     -1      1
#                               3      1      1
#                               3      3      1
#                               3     -2     -1
#                               3      0     -1
#                               3      2     -1
#
# That being said, please now proceed to the program
# -----------------------------------------------------------------------------


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Libraries <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
import os                              # To interact with the OS
import shutil                          # To interact with the file system
import argparse as ap                  # To take command line arguments nicely
from multiprocessing import Pool       # To paralelize jobs

try:                                   # Check if the libraries are there
	import numpy as np                     # For Linear Algebra
	from matplotlib import pyplot as plt   # To plot data
	import matplotlib.cm as cm             # MatPlotLib color palettes
except ImportError as e:               # If not ...
	print(e)
	print("You seem to be missing a library.")
	print("Please check the documentation at: https://gitlab.com/zronyj/lttc-gni")
	quit()


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Functions <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

# -----------------------------------------------------------------------------
# Function: lower_blue_layer
# -----------------------------------------------------------------------------
# Description: This function will generate all blue carbon atoms under a layer
#              of red carbon atoms. Notice that this won't be the last layer.
# Inputs:
#    * layer:      [list]      3-D lists, each representing a red carbon atom
# Outputs:
#    * blue_layer: [list]      3-D lists, each representing a blue carbon atom
# -----------------------------------------------------------------------------
def lower_blue_layer(layer):
	blue_layer = []                # Create an empty list
	for i, a in enumerate(layer):  # Iterate over every red carbon atom
		if a[2] == -1:             # If the carbon atom is NOT red
			raise ValueError("At least one of the values in this layer is a blue atom!")
		if i == 0:                 # If this is the first red carbon atom
			blue_layer.append([a[0], a[1] - 1, -1]) # Create lower LEFT B atom
		blue_layer.append([a[0], a[1] + 1, -1]) # Create lower RIGHT B atom
	return blue_layer


# -----------------------------------------------------------------------------
# Function: lower_blue_closing
# -----------------------------------------------------------------------------
# Description: This function will generate all blue carbon atoms under a layer
#              of red carbon atoms. This WILL be the last layer.
# Inputs:
#    * layer:      [list]      3-D lists, each representing a red carbon atom
# Outputs:
#    * blue_layer: [list]      3-D lists, each representing a blue carbon atom
# -----------------------------------------------------------------------------
def lower_blue_closing(layer):
	blue_layer = []                    # Create an empty list
	for i, a in enumerate(layer[:-1]): # Iterate over every red carbon atom,
		                               # except the last one
		if a[2] == -1:                 # If the carbon atom is NOT red
			raise ValueError("At least one of the values in this layer is a blue atom!")
		blue_layer.append([a[0], a[1] + 1, -1]) # Create lower RIGHT B atom
	return blue_layer


# -----------------------------------------------------------------------------
# Function: lower_red_layer
# -----------------------------------------------------------------------------
# Description: This function will generate all red carbon atoms under a layer
#              of blue carbon atoms.
# Inputs:
#    * layer:      [list]      3-D lists, each representing a blue carbon atom
# Outputs:
#    * red_layer:  [list]      3-D lists, each representing a red carbon atom
# -----------------------------------------------------------------------------
def lower_red_layer(layer):
	red_layer = []                 # Create an empty list
	for i, a in enumerate(layer):  # Iterate over every blue carbon atom
		if a[2] == 1:              # If the carbon atom is NOT blue
			raise ValueError("At least one of the values in this layer is a red atom!")
		red_layer.append([a[0] + 1, a[1], 1]) # Create R atom UNDER blue atom
	return red_layer


# -----------------------------------------------------------------------------
# Function: build_triangle
# -----------------------------------------------------------------------------
# Description: This function will create entries for each atom in a triangulene
#              in a layer by layer fashion. At the beginning, only one red atom
#              is added, then blue and red layers are added in an alternated
#              manner.
# Inputs:
#    * L:          [integer] number of blue carbon atoms on one side of the
#                            triangulene
# Outputs:
#    * triangulene:[list]    3-D lists, each representing a carbon atom
# -----------------------------------------------------------------------------
def build_triangle(L):
	triangulene = [[0,0,1]]             # Create the list and add first element
	temp_red = triangulene[:]           # Copy the list as a red layer
	for i in range(L):                  # Iterate L times
		temp_blue = lower_blue_layer(temp_red) # Create a blue layer under red
		triangulene += temp_blue        # Add the blue layer to the triangulene
		temp_red = lower_red_layer(temp_blue) # Create a red layer under blue
		triangulene += temp_red         # Add the red layer to the triangulene
	triangulene += lower_blue_closing(temp_red) # Add a closing blue layer
	return triangulene


# -----------------------------------------------------------------------------
# Function: rem_lambda
# -----------------------------------------------------------------------------
# Description: This function removes atoms from one of the corners of the
#              triangulene. It does so by removing layers of atoms from the
#              point of view of the corner.
# Inputs:
#    * n:          [integer] number of layers to remove from the corner
#    * L:          [integer] number of blue carbon atoms in one side of the
#                             triangulene
#    * molecule:   [list]    3-D lists, each representing a carbon atom
#    * corner:     [integer] corner from the triangle to remove atoms.
#                             consider the following diagram
#
#                                            2
#                                           / \
#                                          /___\
#                                         0     1
#
# Outputs:
#    * triangulene:[list]    3-D lists, each representing a carbon atom
# -----------------------------------------------------------------------------
def rem_lambda(n, L, molecule, corner=0):
	triangulene = molecule[:]          # Make a copy of the original molecule
	if corner == 0:                    # If the corner is 0 ...
		s = 1                          # ... make my dummy variable s, 1
	elif corner == 1:                  # If the corner is 1 ...
		s = -1                         # ... make my dummy variable s, -1
	elif corner == 2:                  # If the corner is 2 ...
		for i, a in enumerate(triangulene): # ... iterate over every atom ...
			if a[0] == n:              # ... and check whether its layer is the
				                       # same as the number of layer to remove
				start = i              # If so, get the index and stop the loop
				break
		return triangulene[start:]
	else:                              # If the corner is not 0, 1 or 2 ...
		return triangulene             # ... just return the same molecule
	# For the cases of corner=0 or 1 ...
	to_remove = []                     # Create an empty list for atoms to be
	                                   # removed. The number of atoms per layer
	                                   # should be 2*n + 1 where n is the
	                                   # number of the layer.

	for i in range(n):                 # Iterate over every layer
		layer = [[ L - i - 1, s * (i - L), -1 ]] # Create a list of layer atoms
		                                         # and add the first element to
		                                         # be removed from the molecule
		for j in range(i + 1):         # Iterate over the atoms in that layer
			layer.append([ layer[-1][0] + 1, layer[-1][1], 1 ]) # Add red atom
			layer.append([ layer[-1][0], layer[-1][1] + s, -1 ])# Add blue atom
		to_remove += layer             # Concatenate the layer with the list of
		                               # atoms to be removed

	for k in to_remove[::-1]:          # Iterate over every to-be-removed atom
		entry = triangulene.index(k)   # Find that atom in the molecule
		triangulene.pop(entry)         # Remove the entry of that atom from the
		                               # list.
	return triangulene


# -----------------------------------------------------------------------------
# Function: get_coordinates
# -----------------------------------------------------------------------------
# Description: This function computes the coordinates of every atom in a R2
#              space for future plotting.
# Inputs:
#    * molecule:   [list]    3-D lists, each representing a carbon atom
#    * bond_length:[float]   a number representing a bond length
# Outputs:
#    * coords_red: [list]    2-D lists, each with coordinates of a red atom
#    * coords_blue:[list]    2-D lists, each with coordinates of a blue atom
#    * all_coords: [list]    2-D lists, each with the coordinates of an atom
# -----------------------------------------------------------------------------
def get_coordinates(molecule, bond_length=1):
	coords_red = []                   # Create empty list for red atoms coords
	coords_blue = []                  # Create empty list for blue atoms coords
	all_coords = []                   # Create empty list for all atoms coords
	for a in molecule:                # Iterate over the atoms
		x = a[1] * bond_length * np.cos(np.pi / 6) # Compute the x coordinate
		if a[2] == 1:                 # If this is a red atom ...
			                          # ... make the y distance as follows
			ry = -1 * a[0] * bond_length * (np.sin(np.pi / 6) + 1)
			coords_red.append([x, ry]) # Add coordinate to the red atoms list
			all_coords.append([x, ry]) # Add coordinate to the all-atom list
		else:                         # If this is a blue atom ...
			                          # ... make the y distance as follows
			by = -1 * (a[0] + 1) * bond_length * np.sin(np.pi / 6) - a[0] * bond_length
			coords_blue.append([x, by])# Add coordinate to the red atoms list
			all_coords.append([x, by]) # Add coordinate to the all-atom list
	return coords_red, coords_blue, all_coords


# -----------------------------------------------------------------------------
# Function: build_matrix
# -----------------------------------------------------------------------------
# Description: This function will create a Huckel matrix for a triangulene
#              built using the layers system described at the beginning of this
#              program. To do so, it will build the upper right portion of the
#              matrix first and, considering its symmetry, it will add this to
#              its transpose to obtain the full matrix.
# Inputs:
#    * layers:     [list] filled with 3-D lists, each representing a carbon
#                         atom
# Outputs:
#    * huckel + trans_huckel: the Huckel matrix of the given triangulene
# -----------------------------------------------------------------------------
def build_matrix(layers):
	dimension = len(layers)           # Get the number of entries in the matrix
	                                  # ( should be L*(L+4)+1 )
	huckel = np.zeros((dimension, dimension)) # Create the matrix filled with 0
	for i, a in enumerate(layers):    # Iterate over every atom
		huckel[i,i] = 0               # Set the diagonal element equal to 0
		if a[2] == 1:                 # If it's a red atom ...
			blue_left = [a[0], a[1] - 1, -1] # Build coordinates for blue atom
			if blue_left in layers:   # Check if blue atom is in the list
				                      # Compute the bond to the left blue atom
				huckel[i, layers.index(blue_left)] = 1
			blue_right = [a[0], a[1] + 1, -1] # Build coordinates for blue atom
			if blue_right in layers:   # Check if blue atom is in the list
				                      # Compute the bond to the left blue atom
				huckel[i, layers.index(blue_right)] = 1
		else:                         # If it's a blue atom ...
			red_atom = [a[0] + 1, a[1], 1] # Build coordinates for red atom
			if red_atom in layers:    # Check if red atom is in the list
				                      # Compute the bond to the red atom
				huckel[i, layers.index(red_atom)] = 1

	# Note that the idea is to find the atom with the given code and get the
	# index of it in the list of atoms. Then use that index to change the value
	# in the matrix.

	trans_huckel = huckel.transpose() # Compute the transpose of the matrix
	return huckel + trans_huckel


# -----------------------------------------------------------------------------
# Function: save_xyz
# -----------------------------------------------------------------------------
# Description: This function takes the coordinates of all the GNI's atoms
#              and saves them in a text file with the molecular XYZ format.
# Inputs:
#    * L:          [integer] number of blue carbon atoms in one side of the GNI
#    * corners:    [list]    list with the 3 lambdas; layers to remove from
#                            each corner from the triangulene
#    * coords:     [list]    3-D coordinates of the carbon atoms of the GNI
# Outputs:
#    * An XYZ file with the coordinates of the GNI
# -----------------------------------------------------------------------------
def save_xyz(L, corners, coords):
	with open(f"GNI_{L}x{corners[0]}-{corners[1]}-{corners[2]}_coords.xyz", "w") as xyzfil:
		lines = []                  # Create an empty list
		lines.append(f"{len(coords)}\n") # Add the number of atoms
		lines.append(f"Molecular XYZ file created by GNI Generator for ({L}|{corners[0]},{corners[1]},{corners[2]})\n") # TItle
		for a in coords:            # Iterate over all atoms
			                        # Add each atom's coordinates with a format
			lines.append(f"  C {a[0]:>16.8f} {a[1]:>16.8f} {0.:>16.8f}\n")
		xyzfil.writelines(lines)    # Write to file


# -----------------------------------------------------------------------------
# Function: save_huckel_mat
# -----------------------------------------------------------------------------
# Description: This function takes the generated Huckel matrix and saves it
#              into a CSV file to be used by any other program.
# Inputs:
#    * L:          [integer] number of blue carbon atoms in one side of the GNI
#    * corners:    [list]    list with the 3 lambdas; layers to remove from
#                            each corner from the triangulene
#    * h_matrix:   [list]    Huckel matrix for the GNI
# Outputs:
#    * A CSV file with the Huckel matrix for the GNI
# -----------------------------------------------------------------------------
def save_huckel_mat(L, corners, h_matrix):
	with open(f"GNI_{L}x{corners[0]}-{corners[1]}-{corners[2]}_huckel.csv", "w") as hu_mat:
		rows = []                   # Create an empty list
		for row in h_matrix:   # Iterate over the matrix rows
			rows.append("\t".join([f"{e:f}" for e in row]) + "\n") # Add rows
		hu_mat.writelines(rows)     # Write the rows to the file


# -----------------------------------------------------------------------------
# Function: save_eigen
# -----------------------------------------------------------------------------
# Description: This function saves the eigenvalues and eigenvectors of the
#              Huckel matrix into two DAT files respectively.
# Inputs:
#    * L:          [integer] number of blue carbon atoms in one side of the GNI
#    * corners:    [list]    list with the 3 lambdas; layers to remove from
#                            each corner from the triangulene
#    * eigenvals:  [list]    List with all eigenvalues of the Huckel matrix
#    * eigenvecs:  [list]    List with all eigenvectors of the Huckel matrix
# Outputs:
#    * Two DAT files with the eigenvalue and eigenvector data
# -----------------------------------------------------------------------------
def save_eigen(L, corners, eigenvals, eigenvecs):
	# Save the eigenvalues of the Huckel matrix into a file
	with open(f"GNI_{L}x{corners[0]}-{corners[1]}-{corners[2]}_eigvals.dat", "w") as feva:
		feva.writelines([f"{e:.4e} " for e in eigenvals])

	# Save the eigenvectors of the Huckel matrix into a file
	with open(f"GNI_{L}x{corners[0]}-{corners[1]}-{corners[2]}_eigvecs.dat", "w") as feve:
		lines = []                  # Create an empty list
		for row in eigenvecs:       # Iterate over the matrix rows
			lines.append("\t".join([f"{e:.12e}" for e in row]) + "\n")# Add rows
		feve.writelines(lines)      # Write the rows to the file


# -----------------------------------------------------------------------------
# Function: plot_molecule
# -----------------------------------------------------------------------------
# Description: This function takes the coordinates of the red and blue atoms
#              and plots them in a white canvas in order to visualize the
#              triangulene or GNI.
# Inputs:
#    * coords_red:  [list]    3-D coordinates of the red carbon atoms
#    * coords_blue: [list]    3-D coordinates of the blue carbon atoms
# Outputs:
#    * A PNG file named "molecule.png" with the plot of the triangulene or GNI
# -----------------------------------------------------------------------------
def plot_molecule(coords_red, coords_blue):
	RX, RY = zip(*coords_red)        # Split X and Y coordinates for red atoms
	BX, BY = zip(*coords_blue)       # Split X and Y coordinates for blue atoms
	plt.plot(RX, RY, "ro")           # Plot red atoms
	plt.plot(BX, BY, "bo")           # Plot blue atoms
	plt.axis('equal')                # Make the plot square
	plt.axis('off')                  # Remove the axes from the plot
	plt.savefig("molecule.png", dpi=300, bbox_inches="tight") # Save the plot
	plt.close()


# -----------------------------------------------------------------------------
# Function: plot_energies
# -----------------------------------------------------------------------------
# Description: This function plots the eigenvalues of the Huckel matrix in
#              ascending order, and it saves the plot as a PNG file.
# Inputs:
#    * L:          [integer] number of blue carbon atoms in one side of the GNI
#    * corners:    [list]    list with the 3 lambdas; layers to remove from
#                            each corner from the triangulene
#    * eigenvals:  [list]    List with all eigenvalues of the Huckel matrix
#    * show_plot:  [bool]    Choice whether the plot is shown or saved as PNG
# Outputs:
#    * A plot of the energies on screen, or a PNG file named "eigenvals.png"
#      with the plot of eigenvalue energies from the Huckel matrix.
# -----------------------------------------------------------------------------
def plot_energies(L, corners, eigenvals, show_plot=False):
	# Plot the eigenvalues in ascending order
	plt.plot(np.linspace(0,1,len(eigenvals)), eigenvals, "r.-")
	# Set a title for the plot
	plt.title(fr"($\Lambda$|$\lambda_{1}$,$\lambda_{2}$,$\lambda_{3}$) = ({L}|{corners[0]},{corners[1]},{corners[2]})")
	if show_plot:
		plt.show()                      # Just show the plot
	else:
		plt.savefig("eigenvals.png", dpi=300)     # Save the plot
		plt.cla()                       # Clear the plotting area


# -----------------------------------------------------------------------------
# Function: Psi
# -----------------------------------------------------------------------------
# Description: Building a wave function for carbon atoms according to the one
#              described in the following reference:
#              J. Chem. Educ. 2018, 95, 9, 1579–1586
#              https://doi.org/10.1021/acs.jchemed.8b00244
# Inputs:
#    * x:          [float] x coordinate for a given position in 3D space
#    * y:          [float] y coordinate for a given position in 3D space
#    * z:          [float] z coordinate for a given position in 3D space
#    * x0:         [float] x coordinate of a given atom nucleus in 3D
#    * y0:         [float] y coordinate of a given atom nucleus in 3D
#    * z0:         [float] z coordinate of a given atom nucleus in 3D
# Outputs:
#    * wf_val:     [float] The value of the wave function at x, y, z
# -----------------------------------------------------------------------------
def Psi(x, y, z, x0, y0, z0):
	# Radial component of the wave function
	r = ((x - x0)**2 + (y - y0)**2 + (z - z0)**2)**0.5
	# Pre-exponential component of the wave function
	# Effective nuclear charge for carbon = 3.25
	A = (3.25**5 / (32*np.pi))**0.5
	# Exponential component of the wave function
	ex = -3.25 * r / 2
	# The final value of the molecular wave function at point x, y, z
	wf_val = A * (z - z0) * np.e**ex
	return wf_val

# -----------------------------------------------------------------------------
# Function: orbital_plotter2D
# -----------------------------------------------------------------------------
# Description: The function samples the 2D space around the molecule and
#              computes the value of the wave function multiplied by the
#              corresponding eigenvector, leading to the molecular orbital of
#              the GNI.
# Inputs:
#    * coords:      [list]    Coordinates of each atom of the GNI
#    * evals:       [list]    The eigenvalues (energies) of each orbital
#    * evecs:       [list]    The eigenvectors (orbitals) of the GNI
#    * orbital:     [integer] The number of orbital to be plotted
#    * dL:          [float]   The margin used in X and Y around the GNI
#    * delta:       [float]   The grain used in the sampling of 2D space
# Outputs:
#    * A PNG image with the plotted atoms and the respective orbital
# -----------------------------------------------------------------------------
def orbital_plotter2D(coords, evals, evecs,
						z=0.1, orbital=0, dL=1.0, delta=0.05):

	n = len(coords)                    # Get the number of coordinates

	all_X, all_Y = zip(*coords)        # Extract X and Y coordinates separately

	llx = min(all_X)    # Compute the minimum value of the X coordinate
	lly = min(all_Y)    # Compute the minimum value of the Y coordinate
	lux = max(all_X)    # Compute the maximum value of the X coordinate
	luy = max(all_Y)    # Compute the maximum value of the Y coordinate

	ldx = lux - llx     # Compute the current domain of X
	ldy = luy - lly     # Compute the current domain of Y

	dS = ldx - ldy      # Compute the difference between domains

	lower_x = llx - dL  # Establish lower X limit
	upper_x = lux + dL  # Establish upper X limit
	lower_y = lly - dL  # Establish lower Y limit
	upper_y = luy + dL  # Establish upper Y limit

	mid = abs(dS)/2     # Establish how much will the function add on each side

	if dS > 0:          # If X is larger
		lower_y -= mid  # Make Y the same ...
		upper_y += mid  # ... size as X
	elif dS < 0:        # If Y is larger
		lower_x -= mid  # Make X the same ...
		upper_x += mid  # ... size as Y


	dots = int((upper_x - lower_x) / delta) # Get number of dots per domain

	domain_x = np.linspace(lower_x, upper_x, dots) # Create domain for X coords
	domain_y = np.linspace(lower_y, upper_y, dots) # Create domain for Y coords

	xv, yv = np.meshgrid(domain_x, domain_y) # Create grid for the wfn value

	# Compute the influence of the first atom over the molecular orbital
	mol_orb = Psi(xv, yv, z, coords[0][0], coords[0][1], 0) * evecs[0][n - orbital - 1]

	# Computes the influence of each other atom over the molecular orbital
	for i in range(1, n):
		mol_orb += Psi(xv, yv, z, coords[i][0], coords[i][1], 0) * evecs[i][n - orbital - 1]

	# Dirty trick to force the first orbital to behave nicely
	if mol_orb[int(dots/2),int(dots/2)] < 0:
		mol_orb *= -1

	plt.rcParams["figure.figsize"] = 10, 10 # Set the proportion of the plot

	# Plot the orbital as a heat map
	plt.imshow(mol_orb, cmap=cm.winter, extent=[lower_x, upper_x, lower_y, upper_y], origin='lower')

	# Plot the atoms on top of the heat map
	plt.scatter(all_X, all_Y, marker=".", color=(0,0,0,0.1), edgecolors='none')

	plt.grid(False)                    # Remove the grid
	plt.axis('equal')                  # Force the axes to have the same size
	plt.axis('off')                    # Remove the axes

	# Set a title for the plot
	plt.title(f"Orbital {orbital + 1} eigenvalue = {evals[orbital]:.4f}")

	# Save the plot
	plt.savefig(f"orbital_{orbital + 1}.png", dpi=300)

	# Clear the plotting area
	plt.cla()

# -----------------------------------------------------------------------------
# Function: GNI
# -----------------------------------------------------------------------------
# Description: This function takes the dimensions of a triangulene or GNI,
#              makes a new folder for all the new files to be created, builds
#              the triangulene or GNI as a 3-D coordinate system, computes the
#              coordinates for all atoms, creates the Huckel matrix,
#              diagonalizes it, and plots the obtained energy.
#              Additionally, it can save the XYZ file, the matrix, the
#              eigenvalues and eigenvectors, a 2D diagram of the molecule and
#              its orbitals.
# Inputs:
#    * L:           [integer] number of blue carbon atoms on one side of the
#                             triangulene
#    * l1:          [integer] number of layers to remove from the first corner
#    * l2:          [integer] number of layers to remove from the second corner
#    * l3:          [integer] number of layers to remove from the third corner
#    * savexyz:     [boolean] if the XYZ file should be produced
#    * savemat:     [boolean] if the Huckel matrix should be saved into a file
#    * saveeigen:   [boolean] if the eigenvalues and eigenvectors should be
#                             saved into a file
#    * savemol:     [boolean] if a PNG file of the 2D diagram of the molecule
#                             should be saved into a file
#    * showplot:    [boolean] if the plot of the energies should be shown or
#                             saved into a PNG file
#    * orbitals:    [boolean] if the orbitals should be all plotted into PNG
#                             files
#    * force:       [boolean] if the function should force a re-write of
#                             previous data to the HD
# Outputs:
#    * A plot with the eigenenergies of the molecule.
#    * Optional:
#         - An XYZ file with the molecule's coordinates
#         - A file with the Huckel matrix in CSV format
#         - Two files with the eigenvalues and eigenvectors in DAT format 
#         - A plot of the molecule with red and blue atoms
#         - The plots of the molecule's orbitals
# -----------------------------------------------------------------------------
def GNI(L, l1, l2, l3, savexyz=False, savemat=False, saveeigen=False,
		savemol=False, showplot=False, orbitals=False, force=False):

	# ......................... Input error checking ..........................

	# Defining the list of lowercase lambdas
	lam = [0,0,0]

	# Saving the lowercase lambda values into a list
	lam[0] = l1
	lam[1] = l2
	lam[2] = l3

	lam.sort(reverse=True)          # Sort lambda values to get: l1 >= l2 >= l3

	sum_lam = sum(lam)              # Sum all lowercase lambdas
	why = [0]*5                     # Prepare error message

	if sum_lam >= L:                # Check whether lambdas are right
		why[0] = "The sum of all 3 λ[n] can not be greater than or equal to the triangulene dimension Λ.\n\n"
		why[1] = f"\tλ[1] = {l1}, λ[2] = {l2}, λ[3] = {l3}, and Λ = {L}\n"
		why[2] = f"\ttherefore Σ λ[n] = {sum_lam} ≥ {L}\n"
		why[3] = "\twhich would lead to a molecule without electronic delocalization on all\n"
		why[4] = "\tatoms, or a non-existant molecule at all."
		error_message = "".join(why)
		raise ValueError(error_message)

	# ......................... Folder handling part ..........................

	if savexyz or savemat or saveeigen or not(showplot) or orbitals:
		here = os.getcwd()              # Get the current working directory

		dirname = f"GNI_{L}x{lam[0]}-{lam[1]}-{lam[2]}" # Make name for new dir

		working_dir = os.path.join(here,dirname) # Make path for new directory

		if (not os.path.exists(working_dir)):  # Check if directory exists
			os.mkdir(working_dir)              # Create the directory
			os.chdir(working_dir)              # Move to the directory
		elif force:                            # If this should be done by force
			shutil.rmtree(working_dir)         # Remove the directory
			os.mkdir(working_dir)              # Create the directory
			os.chdir(working_dir)              # Move to the directory
		else:                                  # If the directory exists ...
			print("The current triangulene has been calculated already.")
			still = input("Do you wish to continue and re-write it? [y/n] ")
			if still == "y":                   # If the user agrees to go on
				shutil.rmtree(working_dir)     # Remove the directory
				os.mkdir(working_dir)          # Create the directory
				os.chdir(working_dir)          # Move to the directory
			else:                              # The user chose not to continue
				print("Thank you for using HuckEn GNI!\n\nFarewell!")
				quit()                         # End the program here

	# ........................ Create the triangulene .........................

	molecule = build_triangle(L)    # Build the triangulene

	# ....................... Remove corners if needed ........................

	for i in range(3):              # Iterate over the corners
		                            # and remove layers as needed
		if lam[i]: molecule = rem_lambda(lam[i], L, molecule, i)

	# ..................... Turning into real 3-D coords ......................

	# Compute the coordinates of all atoms
	coords_red, coords_blue, all_coords = get_coordinates(molecule, 1.42)

	# Save the triangulene coordinates to an XYZ file
	if savexyz: save_xyz(L, lam, all_coords)

	# Create PNG with blue and red atoms
	if savemol: plot_molecule(coords_red, coords_blue)

	# ............. Creation and diagonalization of Huckel matrix .............

	# Compute the Huckel matrix
	huckel_matrix = build_matrix(molecule)

	# Save the Huckel matrix of the triangulene into a file
	if savemat: save_huckel_mat(L, lam, huckel_matrix)

	# Diagonalize the matrix
	eva, eve = np.linalg.eigh(huckel_matrix)

	# Save the eigenvalues and eigenvectors to two files respectively
	if saveeigen: save_eigen(L, lam, eva, eve)

	# Save a plot of the eigenvalues as energies
	plot_energies(L, lam, eva, showplot)

	# ....................... Plotting of the orbitals ........................

	if orbitals:                    # If the user wants the orbital plots
		orb_plt = os.path.join(working_dir, "orbitals") # Create new path
		os.mkdir(orb_plt)      # Create folder for orbital plots
		os.chdir(orb_plt)      # Go to that folder
		# Make a list with all the inputs for the orbital plotting function
		params = [ [all_coords, eva, eve, 0.1, i, 1.0, 0.05] for i in range(len(eva)) ]
		with Pool() as pool:        # Create pool of processor threads
			# Execute the function over all parameters
			pool.starmap(orbital_plotter2D, params)
		os.chdir(working_dir)       # Come back to the working directory

	if savexyz or savemat or saveeigen or not(showplot) or orbitals:
		os.chdir(here)

	print("Thank you for using HuckEn GNI!\n\nFarewell!")

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Main  Program <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

if __name__ == "__main__":

	# Adding a program description
	desc = """
This program calculates the energy of a triangulene or a graphene
nanoisland -GNI- using a Huckel matrix, and diagonalizes it.
	"""

	# Defining the command line parser, and adding a title to the program
	parser = ap.ArgumentParser(prog="** HuckEn GNI: Triangulene/GNI Huckel Energy Calculator **",
	description=desc,
	epilog="(C) 2023 Rony J. Letona [rony.letona@estudiante.uam.es]")

	# Adding all the command line options to the program
	parser.add_argument('-L', '--shape', type=int, help="Enter the shape of the triangulene", required=True)
	parser.add_argument('-c', '--corner', type=int, nargs=3, help="Enter the number of layers to remove from each corner of the triangulene", required=True)
	parser.add_argument('--show', action='store_true', help="Show the plot of the triangulene/GNI energies without saving the file as a PNG")
	parser.add_argument('--xyz', action='store_true', help="Save an XYZ file with the coordinates of the given triangulene/GNI")
	parser.add_argument('--mat', action='store_true', help="Save a file with the Huckel matrix for the given triangulene/GNI")
	parser.add_argument('--eig', action='store_true', help="Save a file with the Huckel eigenvalues and eigenvectors for the triangulene/GNI")
	parser.add_argument('--mol', action='store_true', help="Save a PNG image with a red-blue dot representation of the triangulene/GNI")
	parser.add_argument('--orb', action='store_true', help="The triangulene/GNI's orbitals as PNG plots for every eigenenergy")
	parser.add_argument('-F', '--force', action='store_true', help="Force saving, even if there's an existing file for this triangulene/GNI")

	# Parsing all the command line arguments
	args = parser.parse_args()

	# Unpacking all 4 integers from the hex command line argument
	l1, l2, l3 = args.corner

	# Computing the triangulene/GNI as requested
	GNI(args.shape, l1, l2, l3,
		savexyz=args.xyz, savemat=args.mat, saveeigen=args.eig,
		savemol=args.mol, showplot=args.show, orbitals=args.orb,
		force=args.force)