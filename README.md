# HuckEn GNI: Triangulene/GNI Huckel Energy Calculator

Last revision: March, 2023

---

This program is the evaluation exercise for the course *TUTORIALS IN THEORETICAL CHEMISTRY* in the EMTCCM.

This program calculates the energy of a triangulene or a graphene nanoisland -GNI- using a Huckel matrix, and diagonalizes it.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Installation

### a) Required software

The program has been compiled and tested using **Python**.

Therefore, I suggest that you run the source code using Python in a unix-like environment. If you don't have such an environment, please have a look at the [Installation](https://docs.anaconda.com/anaconda/install/index.html) article from Anaconda to set up your environment.

To check if you have Python installed in your unix-like environment, please run the following command in a Terminal:

`which python`

If the result isn't a path, please install Python. If you don't want to install Anaconda, please make sure you have the base installation and the required libraries:

- Debian-based Linux: `sudo apt-get install python`
- RedHat-based Linux: `sudo yum install python` or `sudo dnf install python`
- Arch Linux: `sudo pacman -S python`
- macOS: This requires some steps:
  1. `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
  2. `echo 'export PATH="/usr/local/opt/python/libexec/bin:$PATH"' >> ~./profile`
  3. `brew install python`

The libraries are somewhat easier to install once you have the basic Python installation.

- Python base installation:
  1. `pip install numpy`
  2. `pip install scipy`
  3. `pip install matplotlib`

- Anaconda installation:
  1. `conda install -c anaconda numpy`
  2. `conda install -c anaconda scipy`
  3. `conda install -c conda-forge matplotlib`

### b) Source code

The source code for this program should be included with this README, and it should be comprised of the following:

1. *Examples*
  - *GNI_10x0-0-0*
  - *GNI_2x0-0-0*
  - *GNI_3x0-0-0*
  - *GNI_4x0-0-0*
  - *GNI_5x0-0-0*
  - *GNI_6x0-0-0*
  - *GNI_7x0-0-0*
  - *GNI_7x1-0-0*
  - *GNI_7x1-1-0*
  - *GNI_7x1-1-1*
  - *GNI_7x2-0-0*
  - *GNI_7x2-1-0*
  - *GNI_7x2-1-1*
  - *GNI_7x2-2-0*
  - *GNI_7x2-2-1*
  - *GNI_7x2-2-2*
  - *GNI_7x3-0-0*
  - *GNI_7x3-1-0*
  - *GNI_7x3-1-1*
  - *GNI_7x3-2-0*
  - *GNI_7x3-2-1*
  - *GNI_7x3-2-2*
  - *GNI_7x3-3-0*
  - *GNI_7x3-3-1*
  - *GNI_7x3-3-2*
  - *GNI_7x3-3-3*
  - *GNI_7x4-0-0*
  - *GNI_7x4-1-0*
  - *GNI_7x4-1-1*
  - *GNI_7x4-2-0*
  - *GNI_8x0-0-0*
  - *GNI_9x0-0-0* 
2. *Tests*
  - **program_tester.sh**
  - **library_tester.sh**
  - **library_template.py**
3. **huckel_triangulene.py**
4. **README.md**

## 2. Running the program
The program is designed to be used either as a stand-alone program ran from the command line interface -CLI-, or as a Python library from another program/script. In the first case, a comprehensive description on how to run the program can be found by executing the following command:

`./huckel_triangulene.py --help`

The second case requires the user to explore the source code to understand what each function provides. Nevertheless, some examples are given in the following [Colaboratory Notebook](https://colab.research.google.com/drive/1GmsTc1IrBkYDEWo2Jvr-YuWrRWGeuj7G?usp=sharing).

### a) Test
This program was tested in Linux, using an Anaconda environment. More precisely:

- **Linux Mint 21.1 - MATE 64-bit** with a **Linux 5.15.0-60-generic** kernel
- **Python 3.9.13** by **Anaconda, Inc.**
- **NumPy 1.21.5**
- **MatPlotLib 3.5.2**

Please make sure that the version of Python and its libraries is the one described above, or higher. Otherwise, you may encounter some error.

#### i. Command Line Mode

To run all tests for the program using the CLI, please go to the *Tests* folder included in this repository, and run the **program_tester.sh** file. To do so, first grant the file permissions to be executed and then, run the script.

```
chmod 755 program_tester.sh
./program_tester.sh
```

Notice that in order for this script to work, the *Examples* folder has to be in the repository as well!

If all tests are successful, you may use the program without any inconvenience. If at least one test fails, please contact the developer for further assistance.

#### ii. As a library

To run all tests for the program using it as a library, please go to the *Tests* folder included in this repository, and run the **library_tester.sh** file. To do so, first grant the file permissions to be executed and then, run the script.

```
chmod 755 library_tester.sh
./library_tester.sh
```

Notice that in order for this script to work, the *Examples* folder has to be in the repository as well!

If all tests are successful, you may use the program without any inconvenience. If at least one test fails, please contact the developer for further assistance.

### b) Inputs

#### CLI

The program can take several *flags*. Most of them are self explanatory and are shown in by requesting help from the program (as shown in **Section 2**). Nevertheless, a more detailed explanation is provided here:

- `-h` or `--help`    Will show a help message with limited description of each flag.
- `-L` or `--shape`   Gives a triangulene its shape. An integer has to be entered right after the flag, and this integer will represent the number of hexagons on one side of the triangulene, e.g. `./huckel_triangulene.py -L 7 -c 0 0 0` will create a triangulene with the shape (7|0,0,0). This flag is **required** by the program.
- `-c` or `--corner`  Gives the number of layers to be removed from each corner of the triangulene in decreasing order; any other order will be re-arranged by the program. The flag has to be followed by 3 integers separated by a space, e.g. `./huckel_triangulene.py -L 7 -c 3 2 1` will create a triangulene with the shape (7|3,2,1). This flag is **required** by the program.
- `--show`            The plot of the triangulene/GNI energies will be shown during runtime, and not saved as a PNG file, as it usually is.
- `--xyz`             An XYZ file with the coordinates of the given triangulene/GNI will be saved inside a dedicated folder.
- `--mat`             A file with the Huckel matrix for the given triangulene/GNI will be saved in CSV format, inside a dedicated folder.
- `--eig`             Two separate files with the Huckel eigenvalues and eigenvectors for the triangulene/GNI will be saved in DAT format, inside a dedicated folder.
- `--mol`             A PNG image with a red-blue dot representation of the triangulene/GNI will be saved inside a dedicated folder.
- `--orb`             2D images for the triangulene/GNI's orbitals will be saved as PNG plots for every eigenenergy, inside a dedicated folder.
- `-F` or `--force`   Force saving, even if the folder with some or all the previous files exists. Those files will be deleted, and the program will write new files into the folder.


#### Library

When being used as a library, all functions of the program are available for the user. But before that, please keep in mind that the **huckel_triangulene.py** file has to be in the same folder as the program using it as library. Next, when importing it, I personally suggest the idiom: `import huckel_triangulene as gni` so that any function can be run as `gni.some_function()`. I now proceed to give a short list of the basic libraries will be given.

- `build_triangle(L)` - This function will create entries for each atom in a triangulene in a layer by layer fashion. At the beginning, only one red atom is added, then blue and red layers are added in an alternated manner.
  * `L` [integer] number of hexagons in one side of the triangulene
- `rem_lambda(n, L, molecule, corner)` - This function removes atoms from one of the corners of the triangulene. It does so by removing layers of atoms from the point of view of the corner.
  * `n` [integer] number of layers to remove from the corner
  * `L` [integer] number of hexagons in one side of the triangulene
  * `molecule` [list] 3-D vectors, each representing the position and identity of a carbon atom
  * `corner` [integer] corner from the triangle to remove atoms. Can only take values `0` for the lower left corner, `1` for the lower right corner, or `2` for the upper corner
- `build_matrix(molecule)` - This function will create a Huckel matrix for a triangulene built using the layers system described at the beginning. To do so, it will build the upper right portion of the matrix first and, considering its symmetry, it will add this to its transpose to obtain the full matrix.
  * `molecule` [list] 3-D vectors, each representing the position and identity of a carbon atom
- `get_coordinates(molecule, bond_length)` - This function computes the coordinates of every atom in a R2 space for future plotting.
  * `molecule` [list] 3-D vectors, each representing the position and identity of a carbon atom
  * `bond_length` [float] the distance between two carbon atoms (usually 1.42A)
- `save_xyz(L, corners, coords)` - This function takes the coordinates of all the GNI's atoms and saves them in a text file with the molecular XYZ format.
  * `L` [integer] number of hexagons in one side of the triangulene
  * `corners` [list] list with the 3 lambdas; layers to remove from each corner from the triangulene
  * `coords` [list] 3-D coordinates of the carbon atoms of the GNI
- `save_huckel_mat(L, corners, huckel_matrix)` - This function takes the generated Huckel matrix and saves it into a CSV file to be used by any other program.
  * `L` [integer] number of hexagons in one side of the triangulene
  * `corners` [list] list with the 3 lambdas; layers to remove from each corner from the triangulene
  * `huckel_matrix` [list] Huckel matrix for the GNI
- `save_eigen(L, corners, eigenvalues, eigenvectors)` - This function saves the eigenvalues and eigenvectors of the Huckel matrix into two DAT files respectively.
  * `L` [integer] number of hexagons in one side of the triangulene
  * `corners` [list] list with the 3 lambdas; layers to remove from each corner from the triangulene
  * `eigenvalues` [list] List with all eigenvalues of the Huckel matrix
  * `eigenvectors` [list] List with all eigenvectors of the Huckel matrix
- `plot_molecule(coords_red, coords_blue)` - This function takes the coordinates of the red and blue atoms and plots them in a white canvas in order to visualize the triangulene/GNI.
  * `coords_red` [list] 3-D coordinates of the red carbon atoms
  * `coords_blue` [list] 3-D coordinates of the blue carbon atoms
- `plot_energies(L, corners, eigenvalues, show_plot)` - This function plots the eigenvalues of the Huckel matrix in ascending order, and it saves the plot as a PNG file.
  * `L` [integer] number of hexagons in one side of the triangulene
  * `corners` [list] list with the 3 lambdas; layers to remove from each corner from the triangulene
  * `eigenvalues` [list] List with all eigenvalues of the Huckel matrix
  * `show_plot` [bool] Choice whether the plot should be shown or saved as PNG image
- `orbital_plotter2D(coords, evals, evecs, z, orbital, dL, delta)` - The function samples the 2D space around the molecule and computes the value of the wave function multiplied by the corresponding eigenvector, leading to the molecular orbital of the GNI.
  * `coords` [list] 3-D coordinates of the carbon atoms of the GNI
  * `evals` [list] List with all eigenvalues of the Huckel matrix
  * `evecs` [list] List with all eigenvectors of the Huckel matrix
  * `z` [float] Offset distance in the *z* coordinate in order to see the orbitals (by default `=0.1`)
  * `orbital` [integer] The number of orbital to be plotted
  * `dL` [float] The margin used in X and Y around the GNI (by default `=1.0`)
  * `delta` [float] The grain used in the sampling of 2D space (by default `=0.05`)
- `GNI(L, l1, l2, l3, savexyz=False, savemat=False, saveeigen=False, savemol=False, showplot=False, orbitals=False, force=False)` - This function takes the dimensions of a triangulene or GNI, makes a new folder for all the new files to be created, builds the triangulene or GNI as a 3-D coordinate system, computes the coordinates for all atoms, creates the Huckel matrix, diagonalizes it, and plots the obtained energy. Additionally, it can save the XYZ file, the matrix, the eigenvalues and eigenvectors, a 2D diagram of the molecule and its orbitals.
  * `L` [integer] Number of hexagons in one side of the triangulene
  * `l1` [integer] Number of layers to remove from the first corner
  * `l2` [integer] Number of layers to remove from the second corner
  * `l3` [integer] Number of layers to remove from the third corner
  * `savexyz` [boolean] If the XYZ file should be produced
  * `savemat` [boolean] If the Huckel matrix should be saved into a file
  * `saveeigen` [boolean] If the eigenvalues and eigenvectors should be saved into a file
  * `savemol` [boolean] If a PNG file of the 2D diagram of the molecule should be saved into a file
  * `showplot` [boolean] If the plot of the energies should be shown or saved into a PNG file
  * `orbitals` [boolean] If all the orbitals should be saved as PNG image files
  * `force` [boolean] If the function should force a re-write of previous data to the HD

### c) Outputs

The program does not produce any outputs in the CLI mode, except for files. Those files will depend on the user to be saved or not.

When using the program as a library, then the outputs will be those produced by each function; most of them, again, will be files saved in the current working directory.

### d) Examples
The same examples will be carried out in CLI and library modes.

#### CLI

- Create a (12|0,0,0) triangulene, save the coordinates as an XYZ file, and the energy plot as a PNG:
```
./huckel_triangulene.py -L 12 -c 0 0 0 --xyz
```
- Create a (10|2,1,1) triangulene, save the Huckel matrix, the eigenvalues and eigenvectors, but not the energy plot:
```
./huckel_triangulene.py -L 10 -c 2 1 1 --mat --eig --show
```
- Create a (7|2,2,2) triangulene, save the red-blue image of the molecule, the energy plot and the orbitals as PNGs:
```
./huckel_triangulene.py -L 7 -c 2 2 2 --mol --orb
```
(this one might take a while, because of the plotting of the orbitals)
- Create a (9|4,4,0) triangulene, and save everything:
```
./huckel_triangulene.py -L 9 -c 4 4 0 --xyz --mat --eig --mol --orb
```
(this one might also take a while, because of the plotting of the orbitals)

#### Library

I will show how to work with all the functions in the first example, and then proceed with the `GNI` function only.

- Create a (12|0,0,0) triangulene, save the coordinates as an XYZ file, and the energy plot as a PNG:
```
import numpy as np
import huckel_triangulene as gni

mol = gni.build_triangle(12)
coords_red, coords_blue, all_coords = gni.get_coordinates(mol, 1.42)
gni.save_xyz(12, [0,0,0], all_coords)
mat = gni.build_matrix(mol)
eva, eve = np.linalg.eigh(mat)
gni.plot_energies(12, [0,0,0], eva, False)
```
- Create a (10|2,1,1) triangulene, save the Huckel matrix, the eigenvalues and eigenvectors, but not the energy plot:
```
import huckel_triangulene as gni
gni.GNI(10, 2, 1, 1, savemat=True, saveeigen=True, showplot=True)
```
- Create a (7|2,2,2) triangulene, save the red-blue image of the molecule, the energy plot and the orbitals as PNGs:
```
import huckel_triangulene as gni
gni.GNI(7, 2, 2, 2, savemol=True, showplot=False, orbitals=True)
```
- Create a (9|4,4,0) triangulene, and save everything:
```
import huckel_triangulene as gni
gni.GNI(9, 4, 4, 0, savexyz=True, savemat=True, saveeigen=True, savemol=True, showplot=False, orbitals=True)
```

## 3. Theory

The Hückel Hamiltonian consists of the energy of all $`\pi`$ electrons in the molecule. This means that the electrons in $`\sigma`$ bonds are ignored and a shielding effect is used. To construct such a Hamiltonian, the first step is to consider each row and column of the matrix as an atomic index (e.g. $`\hat{H}_{1,2}`$ is the value of energy associated with the bond between atoms 1 and 2).

That being the case, the diagonal elements will have an energy value $`\alpha`$, which represents the **p** orbital's ionization energy. All entries in the matrix representing a $`\pi`$ bond will have an energy vaule $`\beta`$, representing the energy of the bond. Finally, all other entries in the matrix will be $`0`$, since no energy is considered to exist among atoms not sharing a $`\pi`$ bond. Example for benzene:

```math
\hat{H} = \left [ \begin{matrix}
\alpha & \beta & 0 & 0 & 0 & \beta\\ 
\beta & \alpha & \beta & 0 & 0 & 0\\ 
0 & \beta & \alpha & \beta & 0 & 0\\ 
0 & 0 & \beta & \alpha & \beta & 0\\ 
0 & 0 & 0 & \beta & \alpha & \beta\\ 
\beta & 0 & 0 & 0 & \beta & \alpha
\end{matrix} \right ]
```

However, when diagonalizing that matrix, a few algebraic manipulations can be made. Mainly, redefining the expectation value (the energies) as $`\epsilon_i = \frac{E_i - \alpha}{\beta}`$. By doing this, then the Hückel matrix is simplified to a matrix having $`1`$ in the entries where two atoms share a bond, and $`0`$ otherwise.

```math
\left [ \begin{matrix}
\alpha & \beta & 0 & 0 & 0 & \beta\\ 
\beta & \alpha & \beta & 0 & 0 & 0\\ 
0 & \beta & \alpha & \beta & 0 & 0\\ 
0 & 0 & \beta & \alpha & \beta & 0\\ 
0 & 0 & 0 & \beta & \alpha & \beta\\ 
\beta & 0 & 0 & 0 & \beta & \alpha
\end{matrix} \right ] \cdot
\left [ \begin{matrix}
c_1\\ 
c_2\\ 
c_3\\ 
c_4\\ 
c_5\\ 
c_6
\end{matrix} \right ] =
\left [ \begin{matrix}
E_1 & 0 & 0 & 0 & 0 & 0\\ 
0 & E_2 & 0 & 0 & 0 & 0\\ 
0 & 0 & E_3 & 0 & 0 & 0\\ 
0 & 0 & 0 & E_4 & 0 & 0\\ 
0 & 0 & 0 & 0 & E_5 & 0\\ 
0 & 0 & 0 & 0 & 0 & E_6
\end{matrix} \right ] \cdot
\left [ \begin{matrix}
c_1\\ 
c_2\\ 
c_3\\ 
c_4\\ 
c_5\\ 
c_6
\end{matrix} \right ]
```

```math
\left [ \begin{matrix}
\frac{\left ( \alpha - E \right )}{\beta}& 1 & 0 & 0 & 0 & 1\\ 
1 & \frac{\left ( \alpha - E \right )}{\beta} & 1 & 0 & 0 & 0\\ 
0 & 1 & \frac{\left ( \alpha - E \right )}{\beta} & 1 & 0 & 0\\ 
0 & 0 & 1 & \frac{\left ( \alpha - E \right )}{\beta} & 1 & 0\\ 
0 & 0 & 0 & 1 & \frac{\left ( \alpha - E \right )}{\beta} & 1\\ 
1 & 0 & 0 & 0 & 1 & \frac{\left ( \alpha - E \right )}{\beta}
\end{matrix} \right ] \cdot
\left [ \begin{matrix}
c_1\\ 
c_2\\ 
c_3\\ 
c_4\\ 
c_5\\ 
c_6
\end{matrix} \right ] = 0
```

```math
\left [ \begin{matrix}
-\epsilon_1& 1 & 0 & 0 & 0 & 1\\ 
1 & -\epsilon_2 & 1 & 0 & 0 & 0\\ 
0 & 1 & -\epsilon_3 & 1 & 0 & 0\\ 
0 & 0 & 1 & -\epsilon_4 & 1 & 0\\ 
0 & 0 & 0 & 1 & -\epsilon_5 & 1\\ 
1 & 0 & 0 & 0 & 1 & -\epsilon_6
\end{matrix} \right ] \cdot
\left [ \begin{matrix}
c_1\\ 
c_2\\ 
c_3\\ 
c_4\\ 
c_5\\ 
c_6
\end{matrix} \right ] = 0
```

```math
\left [ \begin{matrix}
0 & 1 & 0 & 0 & 0 & 1\\ 
1 & 0 & 1 & 0 & 0 & 0\\ 
0 & 1 & 0 & 1 & 0 & 0\\ 
0 & 0 & 1 & 0 & 1 & 0\\ 
0 & 0 & 0 & 1 & 0 & 1\\ 
1 & 0 & 0 & 0 & 1 & 0
\end{matrix} \right ] \cdot
\left [ \begin{matrix}
c_1\\ 
c_2\\ 
c_3\\ 
c_4\\ 
c_5\\ 
c_6
\end{matrix} \right ] =
\epsilon_i \cdot
\left [ \begin{matrix}
c_1\\ 
c_2\\ 
c_3\\ 
c_4\\ 
c_5\\ 
c_6
\end{matrix} \right ]
```

Translated to our case (i.e. triangulenes and GNIs), we just need an $`n_A`$ by $`n_A`$ matrix filled with zeroes, find those positions where two atoms have a bond, and set those to $`1`$ ($`n_A`$ defined as the number of atoms in the molecule).

## 4. FAQs

1. *Can I run this program in Windows?* - Of course! Python code is cross-platform and way more portable than any other language.

2. *How can I make several GNIs at once?* - In this case, there are two options:
  - You create a bash script to make all the GNIs you need
  - You create a Python script to do the same (hint: use the `GNI` function!)

3. *Can this be run in a Jupyter Notebook?* - Yes, it can. Actually, it was implemented in a Colaboratory Notebook in here for you to test it: https://colab.research.google.com/drive/1GmsTc1IrBkYDEWo2Jvr-YuWrRWGeuj7G?usp=sharing

4. *I deleted an important file. What do I do?* - If you deleted a file, please follow this link to locate the project's repository: https://gitlab.com/zronyj/lttc-gni

## 4. Acknowledgements

Special thanks go to Marlene Bosquez Fuentes for the questions, the moments, and the insight. Your input was very valuable and pushed me forward every time.

Also, thanks to Ania Rodriguez, Amer Alrakik, Juan Carlos del Valle, and Raul Roman for the feedback and ideas; you guys are an amazing team to work with.

## 5. License

Copyright 2023 Rony J. Letona

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
